FROM python:3.10-bullseye

WORKDIR /application

ENV VIRTUAL_ENV=/application/venv
RUN python -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
	
COPY . .

ENTRYPOINT [ "./start.sh" ]