docs = {
    "swagger": "2.0",
    "info": {
        "title": "Absensi Mitramas",
        "description": "API untuk absensi",
        "contact": {
            "email": "rochim.noviyan@gmail.com"
        },
        "version": "0.0.1"
    },
    "host": "20.24.15.143:5000",
    "basePath": "/",
    "tags": [
        {
            "name": "general",
            "description": "Endpoint untuk keperluan umum",
        },
        {
            "name": "user",
            "description": "Endpoint untuk keperluan pengguna",
        },
        {
            "name": "activity",
            "description": "Endpoint untuk keperluan aktivitas"
        },
        {
            "name": "check",
            "description": "Endpoint untuk keperluan absensi"
        }
    ],
    "schemes": [
        "http",
    ],
    "paths": {
        "/": {
            "get": {
                "tags": [
                    "general"
                ],
                "summary": "Cek kondisi API",
                "description": "Mengecek apakah API siap digunakan",
                "operationId": "healthCheck",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "sukses",
                        "schema": {
                            "$ref": "#/definitions/Health"
                        }
                    }
                }
            }
        },
        "/register": {
            "post": {
                "tags": [
                    "user"
                ],
                "summary": "Register pengguna baru",
                "description": "Register pengguna baru",
                "operationId": "register",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Objek pengguna baru",
                        "required": True,
                        "schema": {
                            "$ref": "#/definitions/NewUser"
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "User sukses ditambahkan",
                        "schema": {
                            "$ref": "#/definitions/UserAdded"
                        }
                    },
                    "400": {
                        "description": "Format data bukan json atau terdapat data kosong",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    }
                }
            }
        },
        "/login": {
            "post": {
                "tags": [
                    "user"
                ],
                "summary": "Login pengguna",
                "description": "Login pengguna",
                "operationId": "login",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Data login",
                        "required": True,
                        "schema": {
                            "$ref": "#/definitions/Login"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Berhasil masuk",
                        "schema": {
                            "$ref": "#/definitions/LoggedIn"
                        }
                    },
                    "400": {
                        "description": "Format data bukan json atau terdapat data kosong",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Password salah",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "404": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    }
                }
            }
        },
        "/logout": {
            "get": {
                "tags": [
                    "user"
                ],
                "summary": "Logout pengguna",
                "description": "Logout pengguna dan revoke access token",
                "operationId": "logout",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "204": {
                        "description": "sukses"
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                }
            }
        },
        "/check/in/": {
            "post": {
                "tags": [
                    "check"
                ],
                "summary": "Absensi masuk",
                "description": "Absensi masuk",
                "operationId": "checkIn",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "201": {
                        "description": "Berhasil check in",
                        "schema": {
                            "$ref": "#/definitions/CheckIn"
                        }
                    },
                    "400": {
                        "description": "Pengguna sedang check in",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "500": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeError"
                        }
                    }
                }
            }
        },
        "/check/out/": {
            "post": {
                "tags": [
                    "check"
                ],
                "summary": "Absensi keluar",
                "description": "Absensi keluar",
                "operationId": "checkOut",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "201": {
                        "description": "Berhasil check out",
                        "schema": {
                            "$ref": "#/definitions/CheckIn"
                        }
                    },
                    "400": {
                        "description": "Pengguna sedang check out",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "500": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeError"
                        }
                    }
                }
            }
        },
        "/check": {
            "get": {
                "tags": [
                    "check"
                ],
                "summary": "Ambil riwayat absensi",
                "description": "Ambil riwayat absensi",
                "operationId": "check",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Riwayat absensi berhasil diambil",
                        "schema": {
                            "$ref": "#/definitions/CheckList"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "500": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeError"
                        }
                    }
                }
            }
        },
        "/activity": {
            "get": {
                "tags": [
                    "activity"
                ],
                "summary": "Ambil riwayat aktivitas",
                "description": "Ambil riwayat aktivitas",
                "operationId": "activity",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "date",
                        "in": "query",
                        "description": "Filter tanggal",
                        "required": False,
                        "type": "string",
                        "format": "date",
                        "example": "2022-09-03"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Riwayat aktivitas berhasil diambil",
                        "schema": {
                            "$ref": "#/definitions/ActivityList"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "500": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeError"
                        }
                    }
                }
            },
            "post": {
                "tags": [
                    "activity"
                ],
                "summary": "Menambah aktivitas",
                "description": "Menambah aktivitas baru",
                "operationId": "addActivity",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Aktivitas baru",
                        "required": True,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "activity": {
                                    "type": "string",
                                    "example": "menulis buku"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Berhasil menambah aktivitas baru",
                        "schema": {
                            "$ref": "#/definitions/NewActivity"
                        }
                    },
                    "400": {
                        "description": "Pengguna sedang check out atau data tidak valid",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "500": {
                        "description": "Pengguna tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeError"
                        }
                    }
                }
            }
        },
        "/activity/{activity_id}": {
            "put": {
                "tags": [
                    "activity"
                ],
                "summary": "Mengubah aktivitas",
                "description": "Mengubah aktivitas",
                "operationId": "editActivity",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "activity_id",
                        "in": "path",
                        "description": "id aktivitas yang ingin diubah",
                        "required": True,
                        "type": "integer"
                    },
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Aktivitas",
                        "required": True,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "activity": {
                                    "type": "string",
                                    "example": "menulis sirat"
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "Berhasil mengubah aktivitas baru",
                        "schema": {
                            "$ref": "#/definitions/NewActivity"
                        }
                    },
                    "400": {
                        "description": "Pengguna sedang check out atau data tidak valid",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "404": {
                        "description": "Aktivitas tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    }
                }
            },
            "delete": {
                "tags": [
                    "activity"
                ],
                "summary": "Menghapus aktivitas",
                "description": "Menghapus aktivitas",
                "operationId": "editActivity",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "activity_id",
                        "in": "path",
                        "description": "id aktivitas yang ingin diubah",
                        "required": True,
                        "type": "integer"
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Berhasil mengubah aktivitas baru",
                    },
                    "400": {
                        "description": "Pengguna sedang check out atau data tidak valid",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "403": {
                        "description": "Belum login",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    },
                    "404": {
                        "description": "Aktivitas tidak ditemukan",
                        "schema": {
                            "$ref": "#/definitions/FreeFail"
                        }
                    }
                }
            }
        },
    },
    "definitions": {
        "Health": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "ok"
                }
            }
        },
        "NewUser": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "example": "Rochim Farul"
                },
                "email": {
                    "type": "string",
                    "example": "rochim.noviyan@localhost.dev"
                },
                "password": {
                    "type": "string",
                    "example": "password"
                }
            }
        },
        "UserAdded": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "user": {
                            "$ref": "#/definitions/SerializedUser"
                        }
                    }
                }
            }
        },
        "SerializedUser": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "example": "Rochim Farul"
                },
                "email": {
                    "type": "string",
                    "example": "rochim.noviyan@localhost.dev"
                },
                "id": {
                    "type": "integer",
                    "example": 2
                }
            }
        },
        "Login": {
            "type": "object",
            "properties": {
                "email": {
                    "type": "string",
                    "example": "rochim.noviyan@localhost.dev"
                },
                "password": {
                    "type": "string",
                    "example": "password"
                }
            }
        },
        "FreeError": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "error"
                },
                "message": {
                    "type": "string"
                },
            }
        },
        "FreeFail": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "fail"
                },
                "data": {
                    "type": "object"
                },
            }
        },
        "FreeSuccess": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object"
                },
            }
        },
        "LoggedIn": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "token": {
                            "type": "string"
                        }
                    }
                },
            }
        },
        "CheckIn": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "message": {
                            "type": "string",
                            "example": "berhasil check in"
                        },
                        "user": {
                            "$ref": "#/definitions/SerializedUser"
                        }
                    }
                }
            }
        },
        "CheckList": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "status": {
                            "type": "string",
                            "example": "check in"
                        },
                        "history": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/CheckHistory"
                            }
                        }
                    }
                },
            }
        },
        "CheckHistory": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "integer",
                    "example": 2
                },
                "user_id": {
                    "type": "integer",
                    "example": 1
                },
                "status": {
                    "type": "string",
                    "example": "check in"
                },
                "check_time": {
                    "type": "string",
                    "format": "date-time",
                    "example": "2022-09-03T20:09:39.731120"
                },
            }
        },
        "ActivityList": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "activities": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/ActivityHistory"
                            }
                        }
                    }
                },
            }
        },
        "ActivityHistory": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "integer",
                    "example": 2
                },
                "user_id": {
                    "type": "integer",
                    "example": 1
                },
                "activity": {
                    "type": "string",
                    "example": "dokumentasi"
                },
                "created_at": {
                    "type": "string",
                    "format": "date-time",
                    "example": "2022-09-03T20:09:39.731120"
                },
            }
        },
        "NewActivity": {
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "example": "success"
                },
                "data": {
                    "type": "object",
                    "properties": {
                        "activity": {
                            "$ref": "#/definitions/ActivityHistory"
                        }
                    }
                }
            }
        },
    }
}
