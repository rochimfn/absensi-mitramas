from functools import wraps
from flask import request
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request

from app.models import  RevokedToken, User
from app.response_template import error_response, fail_response


def must_login():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            try:
                verified = verify_jwt_in_request(optional=True)
                if verified is not None:
                    jti = verified[1]['jti']
                    token = RevokedToken.query.filter_by(jti=jti).first()
                    if token is not None:
                        raise Exception
                    return fn(*args, **kwargs)
                else:
                    return error_response('Anda harus login dulu!', 403)
            except Exception as e:
                return error_response('Token tidak valid, silahkan login!', 403)

        return decorator

    return wrapper


def must_check_in():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            logged_user = get_jwt_identity()
            user = User.query.filter_by(id=logged_user['id']).first()
            if not user:
                return error_response('user tidak ditemukan')

            if user.check_in is True:
                return fn(*args, **kwargs)
            else:
                return fail_response({'message': 'belum check in'})
        return decorator

    return wrapper


def must_json():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            if request.is_json:
                return fn(*args, **kwargs)
            else:
                return fail_response({'message': 'data harus dalam format json'})
        return decorator

    return wrapper