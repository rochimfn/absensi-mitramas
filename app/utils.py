def get_empty_and_message(haystack, needle):
    not_valid = {}
    for i in needle:
        if i not in haystack.keys() or haystack.get(i, None) == '':
            not_valid[i] = f'{i} wajib diisi'
    return not_valid