def error_response(message, status_code=500):
    return {'status': 'error', 'message': message}, status_code


def fail_response(data, status_code=400):
    return {'status': 'fail', 'data': data}, status_code


def success_response(data, status_code=200):
    return {'status': 'success', 'data': data}, status_code
