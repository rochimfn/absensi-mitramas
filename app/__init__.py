from flasgger import Swagger
from flask import Flask
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate

from app.blueprints.auth import bcrypt
from app.models import db
from docs import docs


def create_app():
    app = Flask(__name__)
    jwt = JWTManager()
    swagger = Swagger(template=docs)
    app.config.from_pyfile('settings.py')
    db.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)
    swagger.init_app(app)
    migrate = Migrate(app, db)

    from app.blueprints.auth import bp as auth_bp
    app.register_blueprint(auth_bp)

    from app.blueprints.activity import bp as activity_bp
    app.register_blueprint(activity_bp)

    from app.blueprints.check import bp as check_bp
    app.register_blueprint(check_bp)

    @app.route('/')
    def health_check():
        return {'status': 'ok'}

    return app
