from datetime import timedelta
from os import environ 

SQLALCHEMY_DATABASE_URI = environ.get('SQLALCHEMY_DATABASE_URI', 'sqlite:///../database/database.db')
JWT_SECRET_KEY = environ.get('JWT_SECRET_KEY', 'super-secret-key')
JWT_ACCESS_TOKEN_EXPIRES = environ.get('JWT_ACCESS_TOKEN_EXPIRES', timedelta(hours=12))