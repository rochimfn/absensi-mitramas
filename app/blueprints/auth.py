from app.middleware import must_json, must_login
from app.models import RevokedToken, User, db
from app.response_template import fail_response, success_response
from app.utils import get_empty_and_message
from flask import Blueprint, request
from flask_bcrypt import Bcrypt
from flask_jwt_extended import create_access_token, get_jwt

bcrypt = Bcrypt()

bp = Blueprint('auth', __name__, url_prefix='/')


@bp.post('/register/')
@must_json()
def register_user():
    form = request.json
    not_valid = get_empty_and_message(form, ('name', 'email', 'password'))

    if len(not_valid) > 0:
        return fail_response(not_valid)

    new_user = User(
        name=form['name'],
        email=form['email'],
        password=bcrypt.generate_password_hash(form['password'])
    )

    db.session.add(new_user)
    db.session.commit()
    return success_response({'user': new_user.serialize}, 201)


@bp.post('/login/')
@must_json()
def login():
    form = request.json
    not_valid = get_empty_and_message(form, ('email', 'password'))
    if len(not_valid) > 0:
        return fail_response(not_valid)

    user = User.query.filter_by(email=form['email']).first()
    if not user:
        return fail_response({'email': 'email tidak ditemukan'}, 404)
    if not bcrypt.check_password_hash(user.password, form['password']):
        return fail_response({'password': 'password salah'}, 403)

    access_token = create_access_token(identity=user.serialize)
    return success_response({'token': access_token}, 200)


@bp.get('/logout/')
@must_login()
def logout():
    jti = get_jwt()['jti']
    revoked_token = RevokedToken(jti=jti)
    db.session.add(revoked_token)
    db.session.commit()
    return success_response({}, 204)
