from flask import Blueprint, request
from datetime import date, datetime
from flask_jwt_extended import get_jwt_identity
from sqlalchemy import func

from app.middleware import must_check_in, must_json, must_login
from app.models import db, ActivityHistory, User
from app.response_template import error_response, fail_response, success_response
from app.utils import get_empty_and_message

bp = Blueprint('activity', __name__, url_prefix='/activity')


@bp.get('/')
@must_login()
@must_check_in()
def activity():
    logged_user = get_jwt_identity()
    user = User.query.filter_by(id=logged_user['id']).first()

    if not user:
        return error_response('user tidak ditemukan')

    date_args = request.args.get('date', None)
    date_filter = None
    try:
        date_filter = date.fromisoformat(date_args) if date_args else None
    except Exception:
        return fail_response({'date': 'tanggal harus dalam format ISO YYYY-MM-DD'})

    activities = []
    if date_filter:
        user_activity_history = ActivityHistory.query.filter(
            func.date(ActivityHistory.created_at) == date_filter,
            ActivityHistory.user_id == logged_user['id']
        ).all()
        activities = [c.serialize for c in user_activity_history]
    else:
        user_activity_history = ActivityHistory.query.filter_by(
            user_id=logged_user['id']).all()
        activities = [c.serialize for c in user_activity_history]

    return success_response({'activities': activities})


@bp.put('/<int:activity_id>')
@must_login()
@must_check_in()
def edit_activity(activity_id):
    logged_user = get_jwt_identity()
    form = request.json
    not_valid = get_empty_and_message(form, ['activity'])
    if len(not_valid) > 0:
        return fail_response(not_valid)

    activity = ActivityHistory.query.filter_by(
        id=activity_id, user_id=logged_user['id']).first()
    if not activity:
        return fail_response({'id': 'aktivitas tidak ditemukan'}, 404)

    activity.activity = form['activity']
    db.session.commit()

    return success_response({'activity': activity.serialize}, 201)


@bp.post('/')
@must_login()
@must_check_in()
@must_json()
def add_activity():
    logged_user = get_jwt_identity()
    form = request.json
    not_valid = get_empty_and_message(form, ['activity'])
    if len(not_valid) > 0:
        return fail_response(not_valid)

    new_activity = ActivityHistory(
        user_id=logged_user['id'],
        created_at=datetime.now(),
        activity=form['activity']
    )
    db.session.add(new_activity)
    db.session.commit()

    return success_response({'activity': new_activity.serialize}, 201)


@bp.delete('/<int:activity_id>')
@must_login()
@must_check_in()
def delete_activity(activity_id):
    logged_user = get_jwt_identity()

    activity = ActivityHistory.query.filter_by(
        id=activity_id, user_id=logged_user['id']).first()
    if not activity:
        return fail_response({'id': 'aktivitas tidak ditemukan'}, 404)
    db.session.delete(activity)
    db.session.commit()

    return success_response({}, 204)