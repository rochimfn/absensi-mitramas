from flask import Blueprint
from datetime import datetime
from flask_jwt_extended import get_jwt_identity

from app.middleware import must_login
from app.models import db, CheckHistory, User
from app.response_template import error_response, fail_response, success_response

bp = Blueprint('check', __name__, url_prefix='/check')


@bp.post('/in/')
@must_login()
def check_in():
    logged_user = get_jwt_identity()
    user = User.query.filter_by(id=logged_user['id']).first()
    if not user:
        return error_response('user tidak ditemukan')

    if user.check_in is False:
        user.check_in = True
        history = CheckHistory(
            user_id=user.id,
            check_time=datetime.now(),
            status='check in'
        )
        db.session.add(history)
        db.session.commit()
        return success_response({'user': user.serialize,
                                 'message': 'berhasil check in'}, 201)
    else:
        return fail_response({'message': 'user sedang check in'})


@bp.post('/out/')
@must_login()
def check_out():
    logged_user = get_jwt_identity()
    user = User.query.filter_by(id=logged_user['id']).first()
    if not user:
        return error_response('user tidak ditemukan')

    if user.check_in is True:
        user.check_in = False
        history = CheckHistory(
            user_id=user.id,
            check_time=datetime.now(),
            status='check out'
        )
        db.session.add(history)
        db.session.commit()
        return success_response({'user': user.serialize,
                                 'message': 'berhasil check out'}, 201)
    else:
        return fail_response({'message': 'belum check in'})


@bp.get('/')
@must_login()
def check():
    logged_user = get_jwt_identity()
    user = User.query.filter_by(id=logged_user['id']).first()

    if not user:
        return error_response('user tidak ditemukan')

    status = 'check in' if user.check_in == True else 'check out'
    user_check_history = CheckHistory.query.filter_by(
        user_id=logged_user['id']).all()
    history = [c.serialize for c in user_check_history]

    return success_response({'status': status, 'history': history})
