from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    check_in = db.Column(db.Boolean, nullable=False, default=False)
    check_history = db.relationship('CheckHistory', backref='user', lazy=False)

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email
        }


class ActivityHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, nullable=False)
    activity = db.Column(db.String(255), nullable=False)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'created_at': self.created_at.strftime('%Y-%m-%dT%H:%M:%S.%f%z'),
            'activity': self.activity
        }


class CheckHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    check_time = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.String(9), nullable=False)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'check_time': self.check_time.strftime('%Y-%m-%dT%H:%M:%S.%f%z'),
            'status': self.status
        }


class RevokedToken(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(36), nullable=False, index=True)
