#!/bin/bash

. venv/bin/activate
while true; do
    flask db upgrade
    if [[ "$?" == "0" ]]; then
        break
    fi
    sleep 5
done
gunicorn -w 4 -b 0.0.0.0:5000 'app:create_app()'