# Absensi Mitramas

## *Quick Deploy*

This app uses `docker` and `docker-compose`.

0. Clone this repository with `git clone https://gitlab.com/rochimfn/absensi-mitramas.git`.
1. Enter the app directory with `cd absensi-mitramas`,
2. Start the container with `docker-compose up -d`.
3. The HTTP API is served at port 5000 and the documentation is served at path `/apidocs/`.


## *Start Develop*

This app is developed in python version 3.10. Make sure you have it installed.

*Note: This project is developed in Linux based OS. Developing in Windows may need dependencies adjustment.*

0. Clone this repository with `git clone https://gitlab.com/rochimfn/absensi-mitramas.git`.
1. Enter the app directory with `cd absensi-mitramas`,
2. Create a virtual environment for python dependencies with `python -m venv venv` or `python -m virtualenv venv`.
3. Enter the virtual environment with `source venv/bin/activate` (*nix) or `venv/Scripts/activate` (windows).
4. Install all the dependencies with `pip install -r requirements.txt`.
5. You can start the HTTP API with `flask --debug run`.
